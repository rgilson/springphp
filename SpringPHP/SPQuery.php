<?php
/**
 * @author Gilson Rodrigues
 * @version 1.0
 * @package SpringPHP
 * @name SPQuery
 */
class SPQuery {
    /**
     *
     * @var String
     */
    private $schema=NULL;
    /**
     *
     * @var String
     */
    private $select=" * ";
     /**
     *
     * @var String
     */
    private $tableName;
    
    /**
     *
     * @var Array
     */
    private $where=array();
    
    /**
     *
     * @var Array
     */
    private $join=array();
    
    /**
     *
     * @var String
     */
    private $order_by=null;
    /**
     *
     * @var String
     */
    private $group_by=null;
    /**
     * 
     * @param String $schema
     * @param String $tablename
     */
    public function __construct($schema,$tablename) {
        $this->schema=$schema;
        $this->tableName=$tablename;
    } 
    /**
     * 
     * @param String $campos
     */
    public function select($campos) {
        $this->select=$campos;
    }
    /**
     * 
     * @param type $campos
     */
    public function group_by($campos) {
        $this->group_by=$campos;
    }
     /**
     * 
     * @param type $campos
     */
    public function order_by($campos) {
        $this->order_by=$campos;
    }
    /**
     * 
     * @param String $field
     * @param String $value
     * @param String $sinal
     * @param String $ligacao
     */
    public function where($field,$value,$sinal="=",$ligacao=" AND ") {
        $array=$this->where;
        array_push($array,array($field,$value,$sinal,$ligacao));
        $this->where=$array;
    }
    /**
     * 
     * @param type $table
     * @param type $condition
     * @param type $clause
     */
     public function join($table,$condition,$clause="LEFT") {
        $array=$this->join;
        array_push($array,array($table,$condition,$clause));
        $this->join=$array;
    }
    /**
     * 
     * @return string
     */
     private function getJoin(){
        $join = $this->join;
        $query="";
        if (count($join)>0){
            for($x=0;$x<count($join);$x++){
               $query=$query.$join[$x][2].$join[$x][0]." ON ".$join[$x][1];
            }
        }
        return $query;
    } 
    
    /**
     * 
     * @return String
     */
    public function getSelect() {
        if ($this->schema!=null){
           $query = "SELECT ".$this->select." FROM ".$this->schema.".".$this->tableName;
        }else{
           $query = "SELECT ".$this->select." FROM ".$this->tableName; 
        }
        $join = $this->getJoin();
        $query = $query.$join;
        $where = $this->getWhere();
        $query = $query.$where;
        if ($this->order_by!=null){
            $query = $query." ORDER BY ".$this->order_by;
        }
        if ($this->group_by!=null){
            $query = $query." GROUP BY ".$this->group_by;
        }
        return $query;
    }
    /**
     * 
     * @return string
     */
    private function getWhere(){
        $where = $this->where;
        $query="";
        if (count($where)>0){
            $query=$query." WHERE ";
            for($x=0;$x<count($where);$x++){
               $query=$query.$where[$x][0].$where[$x][2]."'".$where[$x][1]."'";
               if ($x<count($where)-1){
                  $query=$query.$where[$x][3];
               } 
            }
        }
        return $query;
    } 
}
