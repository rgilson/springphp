<?php
/**
 * @author Gilson Rodrigues
 * @version 1.0
 * @package SpringPHP
 * @name SPTable
 */
class SPTable {
    /**
     *
     * @var String
     */
    private $schema;
    /**
     *
     * @var Boolean
     */
    public $existe;
    /**
     *
     * @var Bin
     */
    private $conn;
    /**
     *
     * @var String
     */
    private $tableName;
    /**
     *
     * @var List
     */
    public $fields = array();
    /**
     * 
     * @param String $schema
     */
    public function __construct($schema,$connection) {
        $this->schema=$schema;
        $this->conn=$connection;
    }
    /**
     * 
     * @param type $name
     */
    public function setTableName($name) {
        $this->tableName=$name;
        $this->existe=$this->getTableExiste();
    }
    /**
     * 
     */
    public function select() {
        $spquery=new SPQuery($this->schema, $this->tableName);
        $query = $spquery->getSelect();
        $consulta = $this->conn->conexao->query($query);
        $row = $consulta->fetch();
    }
    /**
     * 
     * @return type
     */
    private function getTableExiste() {
        
        $spquery=new SPQuery('INFORMATION_SCHEMA', 'TABLES');
        $spquery->select("count(table_name) existe");
        $spquery->where("table_schema",$this->schema);
        $spquery->where("table_name",$this->tableName);
        
        $query = $spquery->getSelect();
        
        $conexao = $this->conn;
        $consulta = $conexao->query($query);
        $row = $consulta->fetch();
        return $row["existe"]>0;
    }
    /**
     * 
     * @param type $coluna
     * @return type
     */
    private function getColunaExiste($coluna) {
        $spquery=new SPQuery('INFORMATION_SCHEMA', 'COLUMNS');
        $spquery->select("count(COLUMN_NAME) existe");
        $spquery->where("table_schema",$this->schema);
        $spquery->where("table_name",$this->tableName);
        $spquery->where("COLUMN_NAME",$coluna);
        
        $query = $spquery->getSelect();
        
        $conexao = $this->conn;
        $consulta = $conexao->query($query);
        $row = $consulta->fetch();
        return $row["existe"]>0;
    }
    /**
     * 
     */
    public function create() {
        $query= $this->getSqlCreate();
        $conexao = $this->conn;
        $stmt = $conexao->prepare($query);
        $stmt->execute();
    }
    /**
     * 
     */
    public function alter() {
        $conexao = $this->conn;
        $listFields=$this->fields;
        for($x=0;$x<count($listFields);$x++){
            $name=$listFields[$x]["Name"];
            if (!$this->getColunaExiste($name)){
                $query=$this->getSqlAlter($listFields[$x]);
                $stmt = $conexao->prepare($query);
                $stmt->execute();
            }
        }
    }
    /**
     * 
     * @param type $field
     * @return string
     */
    private function getSqlAlter($field){
        $campo = $this->montaCampo($field);
        $query="ALTER TABLE ".$this->tableName." ADD ".$campo;
        return $query;
    }
    /**
     * 
     * @param type $linha
     * @return string
     */
    private function montaCampo($linha){
        $campo="";
        if (array_key_exists("PrimaryKey",$linha)){
           $key="PRIMARY KEY"; 
        }else{
           $key=""; 
        }
        if (array_key_exists("Extra",$linha)){
           $extra="AUTO_INCREMENT"; 
        }else{
           $extra=""; 
        }
        
        if (array_key_exists("Nulls",$linha)){
           if ($linha["Nulls"]=='False'){
              $nulls=" NOT NULL "; 
           }else{
              $nulls="";  
           }
        }else{
           $nulls=""; 
        }
        
        if (array_key_exists("Size",$linha)){
           $campo=$campo.$linha["Name"]." ".$linha["Type"]."(".$linha["Size"].") ".$extra." ".$key.$nulls;
        } else {
           $campo=$campo.$linha["Name"]." ".$linha["Type"]." ".$extra." ".$key.$nulls; 
        }
        
        if (array_key_exists("Default",$linha)){
           $campo=$campo." Default ".$linha["Default"];  
        }
        
        return $campo;
    }
    /**
     * 
     * @return string
     */

    private function getSqlCreate(){
        $sql = "CREATE TABLE IF NOT EXISTS ".$this->tableName."(";
        $listFields=$this->fields;
        
        for($x=0;$x<count($listFields);$x++){
            $sql=$sql.$this->montaCampo($listFields[$x]);
            
            if ($x<count($listFields)-1){
               $sql=$sql.",";
            }else{
               $sql=$sql.")"; 
            }  
        }
        return $sql;
    }
}
