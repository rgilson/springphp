<?php
/**
 * @author Gilson Rodrigues
 * @version 1.0
 * @package SpringPHP
 * @name SPDataBase
 */
class SPDataBase{
    /**
     *
     * @var type PDO
     */
    protected $conexao;
    /**
     *
     * @var type Integer
     */
    public $numExecutes;
    /**
     *
     * @var type Integer
     */
    public $numStatements;
    /**
     *
     * @var Config 
     */
    private $config;
    /**
    * 
    * @param type Config
    */
    public function __construct() {
        $config=new ConfigDataBase();
        $this->config=$config;
        $this->conexao = new PDO($config->getStringConexao(), $config->user, $config->pass, $config->driver);
        $this->numExecutes = 0;
        $this->numStatements = 0;
    }
    
    
   
}

