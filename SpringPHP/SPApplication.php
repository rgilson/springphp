<?php
/**
 * @author Gilson Rodrigues
 * @version 1.0
 * @package SpringPHP
 * @name SPApplication
 */
class SPApplication{
    /**
     *
     * @var type 
     */
    public  $database;
    /**
     *
     * @var type 
     */
    
    public function __construct() {
        $protocolo = (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS']=="on") ? "https" : "http");
        $url = $protocolo.'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $this->database = new SPDataBase();
        $entityFactory=new SPEntityFactory();
        $entityFactory->entityCreate();
    }
    
    public function show() {
        new UserModel();       
    }
    
   
}
?>

