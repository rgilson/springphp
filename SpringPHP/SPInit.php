<?php
spl_autoload_extensions('.php');
spl_autoload_register('loadClasses');
function loadClasses($class) {
    $prefixo = substr($class, 0,2);
    //print $class;
    if ($prefixo=='SP'){
        require_once 'SpringPHP/'.$class.".php";
    }else if ($class=="ConfigDataBase"){    
        require_once 'app/config/'.$class.".php";
    }else{
        if (strpos($class, 'Model')>0){
            require_once 'app/model/'.$class.".php";
        }else if ($class == 'DataBase'){
            require_once 'app/database/'.$class.".php";    
        }else if (strpos($class, 'View')>0){
            require_once 'app/view/'.$class.".php";
        }else if (strpos($class, 'Controller')>0){    
            require_once 'app/controller/'.$class.".php";
        }else{
           require_once 'app/'.$class.".php";
        }
    }
}
?>

