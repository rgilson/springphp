<?php
/**
 * @author Gilson Rodrigues
 * @version 1.0
 * @package SpringPHP
 * @name SPEntityFactory
 */
class SPEntityFactory {
    /**
     *
     * @var type PDO
     */
    protected $conexao;
    
    /**
     *
     * @var Config 
     */
    private $config;
    
    /**
     * 
     */
    public function __construct() {
        $config=new ConfigDataBase();
        $this->config=$config;
        $this->conexao = new PDO($config->getStringConexao(), $config->user, $config->pass, $config->driver);
    }
    /**
     * 
     */
    public function entityCreate(){
        $pastaModels = $_SERVER["DOCUMENT_ROOT"].$_SERVER["REQUEST_URI"]."/app/model/";
        $diretorio = dir($pastaModels);
        while(($arquivo = $diretorio->read()) !== false){
            if ($arquivo<>"."){
                if ($arquivo<>".."){
                    if ($this->isEntity($pastaModels.$arquivo)){
                        $tableName= $this->getTableName($pastaModels,$arquivo);                      
                        $table = new SPTable($this->config->dataBase, $this->conexao);
                        $table->setTableName($tableName);
                        $handle = fopen($pastaModels.$arquivo, 'r');
                        $fields = array();
                        while(!feof($handle)){                      
                            $linha = fgets($handle, 1024);
                            $field = $this->analisaLinha(trim($linha));        
                            if ($field!=null){
                                array_push($fields,$field);
                            }
                        }
                        ;
                        $table->fields=$fields;
                        fclose($handle);
                        $this->createTable($table);
                    }
                }
                
            }
        }
        $diretorio->close();
        
    }
    /**
     * 
     * @param type $nomeArquivo
     * @return boolean
     */
    private function isEntity($nomeArquivo) {
        $lines = file($nomeArquivo); 
        for($x=0;$x<count($lines);$x++) { 
            $linha=$lines[$x];
            if (strpos($linha, "@Entity") >0) {
                return true;
            }
        }
        return false;
    }
    /**
     * 
     * @param type $pastaModels
     * @param type $arquivo
     * @return boolean
     */
    private function getTableName($pastaModels,$arquivo) {
        $className = substr($arquivo,0, strpos($arquivo, "."));                  
        $tableName = substr($className, 0,strpos($className, "Model"));
        $lines = file($pastaModels.$arquivo); 
        for($x=0;$x<count($lines);$x++) { 
            $linha=$lines[$x];
            if (strpos($linha, "@Entity") >0) {
                $x=strpos($linha, "(")+1;
                $y=strpos($linha, ")")-$x;
                $ln = substr($linha,$x,$y);
                $pieces = explode(";", $ln);
                $tableName = substr($pieces[0], strpos($pieces[0],"=")+1);
                break;
            }
        }
        return $tableName;
    }
     /**
      * 
      * @param type $linha
      * @return type
      */  
    private function analisaLinha($linha){
        if (strpos($linha, "@")>0){
            if (strpos($linha, "Field")>0){
                $x=strpos($linha, "(")+1;
                $y=strpos($linha, ")")-$x;
                $ln = substr($linha,$x,$y);
                $pieces = explode(";", $ln);
                $retArray=array();
                for($x=0;$x<count($pieces);$x++){
                    $retArray[substr($pieces[$x],0,strpos($pieces[$x],"="))] = substr($pieces[$x], strpos($pieces[$x],"=")+1);
                }
                return $retArray;
            }
        }
        return null;
    }
    /**
     * 
     * @param type $table
     */
    private function createTable($table) {
        if (!$table->existe){
            $table->create();
        }else{
            $table->alter();
        }    
    }
}
