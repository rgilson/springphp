<?php
/**
 * @author Gilson Rodrigues
 * @version 1.0
 * @package SpringPHP
 * @name SPRepository
 */
class SPRepository {
    /**
     *
     * @var ConfigDataBase 
     */
    private $config;
    /**
     *
     * @var String
     */
    private $pastaModels;
    /**
     *
     * @var String
     */
    private $pastaRepository;
    /**
     * 
     */
    public function __construct() {
        $this->config=new ConfigDataBase();
        $this->pastaModels = $_SERVER["DOCUMENT_ROOT"].$_SERVER["REQUEST_URI"]."/app/model/";
        $this->pastaRepository = $_SERVER["DOCUMENT_ROOT"].$_SERVER["REQUEST_URI"]."/app/repository/";
    }
    
    /**
     * 
     * @param String $className
     * @return String
     */
    private function getTableName($className) {
        $tableName = substr($className, 0,strpos($className, "Model"));
        $lines = file($this->pastaModels.$arquivo); 
        for($x=0;$x<count($lines);$x++) { 
            $linha=$lines[$x];
            if (strpos($linha, "@Entity") >0) {
                $x=strpos($linha, "(")+1;
                $y=strpos($linha, ")")-$x;
                $ln = substr($linha,$x,$y);
                $pieces = explode(";", $ln);
                $tableName = substr($pieces[0], strpos($pieces[0],"=")+1);
                break;
            }
        }
        return $tableName;
    }
    
}
