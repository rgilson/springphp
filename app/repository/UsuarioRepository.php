<?php

/**
 * @author Gilson Rodrigues
 * @version 1.0
 * @package Vishi
 * @name UsuarioRepository
 * @Repository(Entity=UserModel)
 */
class UsuarioRepository  extends SPRepository{
     /**
      *
      * @var SPQuery
      */
    private $query;
    /**
      *
      * @var String
      */
    private $tablename;
    
    public function __construct() {
        parent::__construct();
        $this->tablename = $this->getTableName();
        $this->query=new SPQuery($this->config->dataBase, $this->tablename);
    }
    
}
