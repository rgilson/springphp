<?php
/**
 * @author Gilson Rodrigues
 * @version 1.0
 * @package SpringPHP
 * @name Config
 */
class ConfigDataBase{
    /**
     *
     * @var String 
     */
    private $sgdb="mysql";
    /**
     *
     * @var String 
     */
    private $host = "localhost";
    //private $host = "mysql:host=localhost;dbname=vishicom_recruta";
    /**
     *
     * @var String 
     */
    public $dataBase = "vishi";
    //public $dataBase = "vishicom_recruta";         
    /**
     *
     * @var String 
     */
    public $user = "root";
    //public $user = "vishicom_recruta";
    /**
     *
     * @var String 
     */
    public $pass = "";
    //public $pass = "47978eS082520@";
    /**
     *
     * @var String 
     */
    public $driver =  null;
    /**
     * 
     * @return String
     */
    public function getStringConexao(){
        return $this->sgdb.":host=".$this->host.";dbname=".$this->dataBase;
    }
}

