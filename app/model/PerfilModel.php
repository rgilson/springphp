<?php
/**
 * @author Gilson Rodrigues
 * @name PerfilModel 
 * @version 1.0
 * @package SpringPHP
 * @Entity(TableName=SP_PERFIL;PrimaryKey=PER_ID)
 */
class PerfilModel {
    /**
     * @var Long 
     * @Field(Name=PER_ID;Type=BigInt;Extra=AUTO_INCREMENT;PrimaryKey=True)
     */
    public $ferfilId;
    
    /**
     * @var String 
     * @Field(Name=PER_CODIGO;Type=VarChar;Size=6;Nulls=False)
     */
    public $perfilCodigo;
    
    /**
     * @var String 
     * @Field(Name=PER_NAME;Type=VarChar;Size=20;Nulls=False)
     */
    public $perfilName;
    
    /**
     * @var String 
     * @Field(Name=PER_STATUS;Type=VarChar;Size=1;Nulls=False;Default='S')
     */
    public $perfilStatus;
}
