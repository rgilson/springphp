<?php
/**
 * @author Gilson Rodrigues
 * @name MetodoModel 
 * @version 1.0
 * @package SpringPHP
 * @Entity(TableName=SP_METODO;PrimaryKey=MET_ID)
 */
class MetodoModel extends SPModel{
    /**
     * @var Long 
     * @Field(Name=MET_ID;Type=BigInt;Extra=AUTO_INCREMENT;PrimaryKey=True)
     */
    public $metodoId;
    
    /**
     * @var String 
     * @Field(Name=MET_NAME;Type=VarChar;Size=20;Nulls=False)
     */
    public $metodoName;  
    
    /**
     * @var String 
     * @Field(Name=MET_DESCRICAO;Type=VarChar;Size=80;Nulls=False)
     */
    public $metodoDescricao;
    
    /**
     * @var String 
     * @Field(Name=MET_STATUS;Type=VarChar;Size=1;Nulls=False;Default='A')
     */
    public $metodoStatus;
}
