<?php
/**
 * @author Gilson Rodrigues
 * @name UserModel
 * @version 1.0
 * @package SpringPHP
 * @Entity(TableName=SP_USUARIO;PrimaryKey=USER_ID)
 */
class UserModel  extends SPModel{
    /**
     * @var Long 
     * @Field(Name=USER_ID;Type=BigInt;Extra=AUTO_INCREMENT;PrimaryKey=True)
     */
    public $userId;
    
    /**
     * @var String 
     * @Field(Name=USER_NAME;Type=VarChar;Size=20;Nulls=False)
     */
    public $userName;
    
    /**
     * @var String 
     * @Field(Name=USER_EMAIL;Type=VarChar;Size=40;Nulls=False)
     */
    public $userEmail;
    
    /**
     * @var String 
     * @Field(Name=USER_STATUS;Type=VarChar;Size=1;Nulls=False;Default='A')
     */
    public $userStatus;
    
    
}

