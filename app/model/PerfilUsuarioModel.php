<?php
/**
 * @author Gilson Rodrigues
 * @name PerfilUsuarioModel 
 * @version 1.0
 * @package SpringPHP
 * @Entity(TableName=SP_PERFIL_USUARIO;PrimaryKey=PU_ID)
 */
class PerfilUsuarioModel {
    /**
     * @var Long 
     * @Field(Name=PU_ID;Type=BigInt;Extra=AUTO_INCREMENT;PrimaryKey=True)
     */
    public $puId;
    
    /**
     * @var BigInt 
     * @Field(Name=PU_PER_ID;Type=BigInt;Nulls=False)
     */
    public $perfilId;
    
     /**
     * @var BigInt 
     * @Field(Name=PU_USER_ID;Type=BigInt;Nulls=False)
     */
    public $usuarioId;
    
    
    /**
     * @var String 
     * @Field(Name=PU_STATUS;Type=VarChar;Size=1;Nulls=False;Default='S')
     */
    public $perfilStatus;
}
